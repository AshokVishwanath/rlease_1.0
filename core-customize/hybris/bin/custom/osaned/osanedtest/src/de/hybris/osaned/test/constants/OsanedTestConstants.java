/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.osaned.test.constants;

/**
 * 
 */
public class OsanedTestConstants extends GeneratedOsanedTestConstants
{

	public static final String EXTENSIONNAME = "osanedtest";

}
