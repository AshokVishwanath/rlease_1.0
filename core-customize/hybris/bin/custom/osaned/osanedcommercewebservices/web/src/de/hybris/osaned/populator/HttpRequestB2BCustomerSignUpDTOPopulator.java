/**
 *
 */
package de.hybris.osaned.populator;

import de.hybris.platform.commercewebservicescommons.dto.user.B2BCustomerSignUpWsDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;


/**
 * @author bala.v
 *
 */
@Component("HttpRequestB2BCustomerSignUpDTOPopulator")
public class HttpRequestB2BCustomerSignUpDTOPopulator implements Populator<HttpServletRequest, B2BCustomerSignUpWsDTO>
{
	private static final String EMAIL = "email";
	private static final String NAME = "name";
	private static final String CONTACT = "contact";
	private static final String COMPANY = "company";
	private static final String SIZERANGE = "sizeRange";
	private static final String COMMERCIALREGISTRATION = "commercialRegistration";

	@Override
	public void populate(final HttpServletRequest source, final B2BCustomerSignUpWsDTO target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setEmail(source.getParameter(EMAIL));
		target.setName(source.getParameter(NAME));
		target.setContact(source.getParameter(CONTACT));
		target.setCompany(source.getParameter(COMPANY));
		target.setSizeRange(source.getParameter(SIZERANGE));
		target.setCommercialRegistration(source.getParameter(COMMERCIALREGISTRATION));

	}

}
