<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
<div class="container">
<div class="row" data-role="content">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3 mb-30 clearfix">
      <div class="register__section">
		<div class="col-xs-12 text-center">
		<h1 class="text-center">Please Register Here !!!</h1>
<!-- 		         <div class="headline">
		            New Customer Registration
		         </div> 
		         <p>
		            Submit your registration below
		         </p> -->
		</div>
         <div>
            <form id="registerForm" action="/yb2bacceleratorstorefront/powertools/en/USD/register" method="post">
               <!-- <div class="form-group">
                  <label class="control-label " for="address.country_del">
                  Country<span class="mandatory">
                  </span>
                  <span class="skip">
                  </span>
                  </label>
                  <div class="control">
                     <select id="address.country_del" name="companyAddressCountryIso" class="form-control">
                        <option value="" disabled="disabled" selected="selected">
                           Please select a country
                        </option>
                        <option value="AD">Andorra</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="AF">Afghanistan</option>
                        <option value="AG">Antigua and Barbuda</option>
                        <option value="AI">Anguilla</option>
                        <option value="AL">Albania</option>
                        <option value="AM">Armenia</option>
                        <option value="AO">Angola</option>
                        <option value="AQ">Antarctica</option>
                        <option value="AR">Argentina</option>
                        <option value="AS">American Samoa</option>
                        <option value="AT">Austria</option>
                        <option value="AU">Australia</option>
                        <option value="AW">Aruba</option>
                        <option value="AX">Aland Islands</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BA">Bosnia and Herzegovina</option>
                        <option value="BB">Barbados</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BE">Belgium</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BH">Bahrain</option>
                        <option value="BI">Burundi</option>
                        <option value="BJ">Benin</option>
                        <option value="BL">Saint Barth�lemy</option>
                        <option value="BM">Bermuda</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BO">Bolivia</option>
                        <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                        <option value="BR">Brazil</option>
                        <option value="BS">Bahamas</option>
                        <option value="BT">Bhutan</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BW">Botswana</option>
                        <option value="BY">Belarus</option>
                        <option value="BZ">Belize</option>
                        <option value="CA">Canada</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CF">Central African Republic</option>
                        <option value="CG">Congo</option>
                        <option value="CH">Switzerland</option>
                        <option value="CI">Cote d'Ivoire</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CL">Chile</option>
                        <option value="CM">Cameroon</option>
                        <option value="CN">China</option>
                        <option value="CO">Colombia</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CU">Cuba</option>
                        <option value="CV">Cape Verde</option>
                        <option value="CW">Cura�ao</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DE">Germany</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DK">Denmark</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="DZ">Algeria</option>
                        <option value="EC">Ecuador</option>
                        <option value="EE">Estonia</option>
                        <option value="EG">Egypt</option>
                        <option value="EH">Western Sahara</option>
                        <option value="ER">Eritrea</option>
                        <option value="ES">Spain</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FI">Finland</option>
                        <option value="FJ">Fiji</option>
                        <option value="FK">Falkland Islands</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FR">France</option>
                        <option value="GA">Gabon</option>
                        <option value="GB">United Kingdom</option>
                        <option value="GD">Grenada</option>
                        <option value="GE">Georgia</option>
                        <option value="GF">French Guiana</option>
                        <option value="GG">Guernsey</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GL">Greenland</option>
                        <option value="GM">Gambia</option>
                        <option value="GN">Guinea</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="GR">Greece</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="GT">Guatemala</option>
                        <option value="GU">Guam</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HM">Heard Island and McDonald Islands</option>
                        <option value="HN">Honduras</option>
                        <option value="HR">Croatia</option>
                        <option value="HT">Haiti</option>
                        <option value="HU">Hungary</option>
                        <option value="ID">Indonesia</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IM">Isle of Man</option>
                        <option value="IN">India</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="IQ">Iraq</option>
                        <option value="IR">Iran</option>
                        <option value="IS">Iceland</option>
                        <option value="IT">Italy</option>
                        <option value="JE">Jersey</option>
                        <option value="JM">Jamaica</option>
                        <option value="JO">Jordan</option>
                        <option value="JP">Japan</option>
                        <option value="KE">Kenya</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="KH">Cambodia</option>
                        <option value="KI">Kiribati</option>
                        <option value="KM">Comoros</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LB">Lebanon</option>
                        <option value="LC">Saint Lucia</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="LR">Liberia</option>
                        <option value="LS">Lesotho</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="LV">Latvia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="MA">Morocco</option>
                        <option value="MC">Monaco</option>
                        <option value="MD">Moldova</option>
                        <option value="ME">Montenegro</option>
                        <option value="MF">Saint Martin (French part)</option>
                        <option value="MG">Madagascar</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MK">Macedonia</option>
                        <option value="ML">Mali</option>
                        <option value="MM">Myanmar</option>
                        <option value="MN">Mongolia</option>
                        <option value="MO">Macao</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MS">Montserrat</option>
                        <option value="MT">Malta</option>
                        <option value="MU">Mauritius</option>
                        <option value="MV">Maldives</option>
                        <option value="MW">Malawi</option>
                        <option value="MX">Mexico</option>
                        <option value="MY">Malaysia</option>
                        <option value="MZ">Mozambique</option>
                        <option value="NA">Namibia</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NE">Niger</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="NG">Nigeria</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NL">Netherlands</option>
                        <option value="NO">Norway</option>
                        <option value="NP">Nepal</option>
                        <option value="NR">Nauru</option>
                        <option value="NU">Niue</option>
                        <option value="NZ">New Zealand</option>
                        <option value="OM">Oman</option>
                        <option value="PA">Panama</option>
                        <option value="PE">Peru</option>
                        <option value="PF">French Polynesia</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PH">Philippines</option>
                        <option value="PK">Pakistan</option>
                        <option value="PL">Poland</option>
                        <option value="PM">Saint Pierre and Miquelon</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="PS">Palestinian Territory</option>
                        <option value="PT">Portugal</option>
                        <option value="PW">Palau</option>
                        <option value="PY">Paraguay</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RS">Serbia</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SC">Seychelles</option>
                        <option value="SD">Sudan</option>
                        <option value="SE">Sweden</option>
                        <option value="SG">Singapore</option>
                        <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="SI">Slovenia</option>
                        <option value="SJ">Svalbard and Jan Mayen</option>
                        <option value="SK">Slovakia</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SM">San Marino</option>
                        <option value="SN">Senegal</option>
                        <option value="SO">Somalia</option>
                        <option value="SR">Suriname</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SV">El Salvador</option>
                        <option value="SX">Sint Maarten (Dutch part)</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="SZ">Swaziland</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TD">Chad</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="TG">Togo</option>
                        <option value="TH">Thailand</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TK">Tokelau</option>
                        <option value="TL">Timor-Leste</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TN">Tunisia</option>
                        <option value="TO">Tonga</option>
                        <option value="TR">Turkey</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TV">Tuvalu</option>
                        <option value="TW">Taiwan</option>
                        <option value="TZ">Tanzania</option>
                        <option value="UA">Ukraine</option>
                        <option value="UG">Uganda</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="US">United States</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="VE">Venezuela</option>
                        <option value="VG">Virgin Islands, British</option>
                        <option value="VI">Virgin Islands, U.S.</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VU">Vanuatu</option>
                        <option value="WF">Wallis and Futuna</option>
                        <option value="WS">Samoa</option>
                        <option value="YE">Yemen</option>
                        <option value="YT">Mayotte</option>
                        <option value="ZA">South Africa</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
                     </select>
                  </div>
               </div> -->
               <div class="form-group">
                  <label class="control-label " for="register.title">
                  Title<span class="mandatory">
                  </span>
                  <span class="skip">
                  </span>
                  </label>
                  <div class="control">
                     <select id="register.title" name="titleCode" class="form-control form-border-left">
                        <option value="" selected="selected">
                           None
                        </option>
                        <option value="mr">Mr</option>
                        <option value="mrs">Mrs</option>
                        <option value="miss">Miss</option>
                        <option value="ms">Ms</option>
                        <option value="dr">Dr.</option>
                        <option value="rev">Rev.</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label " for="text.secureportal.register.firstName">
                  First Name</label>
                  <input id="text.secureportal.register.firstName" name="firstName" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="text.secureportal.register.lastName">
                  Last Name</label>
                  <input id="text.secureportal.register.lastName" name="lastName" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="text.secureportal.register.companyName">
                  Company Name</label>
                  <input id="text.secureportal.register.companyName" name="companyName" class="form-control form-control form-border-left" type="text" value="">
               </div>
              <!--  <div class="form-group">
                  <label class="control-label " for="address.line1">
                  Address Line 1</label>
                  <input id="address.line1" name="companyAddressStreet" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="address.line2">
                  Address Line 2<span>&nbsp;(optional)</span>
                  </label>
                  <input id="address.line2" name="companyAddressStreetLine2" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="address.townCity">
                  Town/City</label>
                  <input id="address.townCity" name="companyAddressCity" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="address.postcode">
                  Postcode</label>
                  <input id="address.postcode" name="companyAddressPostalCode" class="form-control form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label " for="text.secureportal.register.position">
                  Your Position</label>
                  <input id="text.secureportal.register.position" name="position" class="form-control form-control form-border-left" type="text" value="">
               </div> -->
               <div class="row">
                  <div class="col-sm-10 phone">
                     <div class="form-group">
                        <label class="control-label " for="storeDetails.table.telephone">
                        Telephone</label>
                        <input id="storeDetails.table.telephone" name="telephone" class="form-control form-control form-border-left" type="text" value="">
                     </div>
                  </div>
                  <div class="col-sm-2 extension">
                     <div class="form-group">
                        <label class="control-label " for="text.secureportal.register.extension">
                        Country</label>
                        <input id="text.secureportal.register.extension" name="telephoneExtension" class="form-control form-control form-border-left" type="text" value="">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label " for="register.email">
                  Email</label>
                  <input id="register.email" name="email" class="form-control js-secureportal-orignal-register-email form-control form-border-left" type="text" value="">
               </div>
               <div class="form-group">
                  <label class="control-label" for="register.confirm.email"> Confirm Email Address</label>
                  <input class="form-control js-secureportal-confirm-register-email form-border-left" id="register.confirm.email">
                  <div class="js-secureportal-email-not-match-message has-error" style="display:none">
                     <span class="help-block">
                     Email's don't match</span>
                  </div>
               </div>
<!--                <div class="form-group">
                  <label class="" for="text.secureportal.register.message">
                  Comment<span>&nbsp;(optional)</span>
                  <span class="skip"></span>
                  </label>
                  <textarea id="text.secureportal.register.message" name="message" class="textarea form-control form-border-left"></textarea>
               </div> -->
               <input type="hidden" id="recaptchaChallangeAnswered" value="">
               <div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
               <div class="register-form-action row">
                  <div class="col-xs-12 col-md-12">
                     <button data-theme="b" class="js-secureportal-register-button btn btn-primary btn-block">
                     Register</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="col-sm-2">
      <div class="item_container">
         <div class="yCmsContentSlot side-content-slot cms_disp-img_slot"></div>
      </div>
   </div>
</div>
</div>
</template:page>