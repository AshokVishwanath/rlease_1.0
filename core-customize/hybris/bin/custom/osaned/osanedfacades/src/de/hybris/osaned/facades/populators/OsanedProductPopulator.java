package de.hybris.osaned.facades.populators;



import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;




public class OsanedProductPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		Assert.notNull(source, "ProductModel is null");
		target.setPicture(setpicture(source, target));
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private MediaData setpicture(final ProductModel source, final ProductData target)
	{
		if (null != source.getPicture())
		{
			final MediaModel mediaModel = source.getPicture();
			final MediaData mediaData = new MediaData();
			mediaData.setCode(mediaModel.getCode());
			mediaData.setAltText(mediaModel.getAltText());
			mediaData.setUrl(mediaModel.getUrl());
			return mediaData;
		}
		return null;

	}

}
