/**
 *
 */
package de.hybris.osaned.facades.customer;

import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author bala.v
 *
 */
public interface OsanedB2BCustomerFacade
{
	/**
	 * @param b2bCustomerData
	 * @throws DuplicateUidException
	 */
	public void registerB2BCustomer(B2BCustomerData b2bCustomerData) throws DuplicateUidException;
}
