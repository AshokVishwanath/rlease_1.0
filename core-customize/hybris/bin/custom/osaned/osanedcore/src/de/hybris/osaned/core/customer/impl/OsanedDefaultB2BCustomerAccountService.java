/**
 *
 */
package de.hybris.osaned.core.customer.impl;

import de.hybris.osaned.core.customer.OsanedB2BCustomerAccountService;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.core.model.user.UserGroupModel;

import java.util.Collections;
import java.util.Random;


/**
 * @author bala.v
 *
 */
public class OsanedDefaultB2BCustomerAccountService extends DefaultB2BCustomerAccountService
		implements OsanedB2BCustomerAccountService
{
	private B2BUnitService b2bUnitService;
	private static final String DEFAULTPASSWORD = "Test@1234";

	/**
	 * registerB2BCustomer
	 */
	@Override
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getModelService().create(B2BCustomerModel.class);
		b2bCustomerModel.setUid(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setContact(b2bCustomerData.getContact());
		b2bCustomerModel.setCompany(b2bCustomerData.getCompany());
		b2bCustomerModel.setSizeRange(b2bCustomerData.getSizeRange());
		b2bCustomerModel.setPassword(DEFAULTPASSWORD);
		b2bCustomerModel.setActive(Boolean.FALSE);
		b2bCustomerModel.setCommercialRegistration(b2bCustomerData.getCommercialRegistration());
		final UserGroupModel b2bCustomerGroup = getUserService().getUserGroupForUID(B2BConstants.B2BCUSTOMERGROUP);
		b2bCustomerModel.setDefaultB2BUnit(createB2BUnit(b2bCustomerData));
		b2bCustomerModel.setGroups(Collections.singleton(b2bCustomerGroup));
		getModelService().save(b2bCustomerModel);
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	private B2BUnitModel createB2BUnit(final B2BCustomerData b2bCustomerData)
	{
		final B2BUnitModel b2bUnitModel = getModelService().create(B2BUnitModel.class);
		final Random random = new Random();
		final int randonumber = random.nextInt(10000);
		b2bUnitModel.setUid(b2bCustomerData.getName() + b2bCustomerData.getCompany() + randonumber);
		b2bUnitModel.setName(b2bCustomerData.getName() + b2bCustomerData.getCompany() + randonumber);
		b2bUnitModel.setGroups(Collections.singleton(getParentUnit()));
		getModelService().save(b2bUnitModel);
		return b2bUnitModel;

	}

	/**
	 * @param uid
	 * @return
	 */
	private B2BUnitModel getParentUnit()
	{
		final CompanyModel companyModel = getB2bUnitService().getUnitForUid("ParentUnit");
		if (null != companyModel)
		{
			if (companyModel instanceof B2BUnitModel)
			{
				return (B2BUnitModel) companyModel;
			}
		}
		return null;
	}

	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}

}
