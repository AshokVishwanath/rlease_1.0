/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.platform.commercefacades.user.data.B2BCustomerData;


/**
 * @author bala.v
 *
 */
public interface OsanedB2BCustomerAccountService
{

	/**
	 * @param b2bCustomerData
	 */
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData);

}
