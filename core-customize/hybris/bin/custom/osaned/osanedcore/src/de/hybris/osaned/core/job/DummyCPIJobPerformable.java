
package de.hybris.osaned.core.job;

import de.hybris.osaned.core.model.DummyCPIJobModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import net.minidev.json.JSONObject;


/**
 * @author Aswin G
 *
 */
public class DummyCPIJobPerformable extends AbstractJobPerformable<DummyCPIJobModel>
{

	private static final Logger LOG = Logger.getLogger(DummyCPIJobPerformable.class);

	@Override
	public PerformResult perform(final DummyCPIJobModel arg0)
	{
		try
		{
			final String username = "S0016534031";
			final String password = "Obeikan@123";

			final String encoded = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
			final URL url = new URL("https://e650071-iflmap.hcisbt.sa1.hana.ondemand.com/http/SAPCommerceCloud");
			final HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Authorization", "Basic " + encoded);
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			httpURLConnection.setRequestProperty("Accept", "application/json");
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.connect();
			final JSONObject obj = new JSONObject();
			obj.put("FirstName", "Bala");
			obj.put("LastName", "Murugan");
			final String data = obj.toString();
			//Write
			final OutputStream outputStream = httpURLConnection.getOutputStream();
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
			writer.write(data);
			writer.close();
			outputStream.close();
			final int responseCode = httpURLConnection.getResponseCode();
			if (responseCode == HttpsURLConnection.HTTP_OK)
			{
				//Read
				final BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

				String line = null;
				final StringBuilder sb = new StringBuilder();

				while ((line = bufferedReader.readLine()) != null)
				{
					sb.append(line);
				}

				bufferedReader.close();
				LOG.info(sb.toString());
			}
			else
			{
				//    return new String("false : "+responseCode);
				LOG.info(new String("false : " + responseCode));

			}
		}
		catch (final MalformedURLException e)
		{
			// XXX Auto-generated catch block
			e.printStackTrace();
		}
		catch (final IOException e)
		{
			// XXX Auto-generated catch block
			e.printStackTrace();
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}



